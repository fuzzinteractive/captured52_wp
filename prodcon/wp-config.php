<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fuzzllc_cap52_wp');

/** MySQL database username */
define('DB_USER', 'fuzzllc_cap52_wp');

/** MySQL database password */
define('DB_PASSWORD', 'y8eDrDZR');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'GFeRdAfqomaFhDN26VqndrZnMisqjBwgK46Jde37HamKzKV4qD4NALM1Nlfg3n2G');
define('SECURE_AUTH_KEY', 'eHk1VUg3PRsmODvdoEkbHsb75XBUUoXx05Wkul8X6zcNJ7J5N60aYel9d0uRHoF9');
define('LOGGED_IN_KEY', '6D5o7RYG1fafERH0FcgOGgjIHcm5cxQj94Z9uWFZi5GOxt6PlIlGQjzSOkoEqVfC');
define('NONCE_KEY', 'HYxyhjeX4ddMjf5KEKLHw81F50vAYTTWqTpHcXK5KOd6YvqlYNp3j3qME9R2hzPz');
define('AUTH_SALT', 'jMTwraBu4VEhTpJEEhROcOk24NX4u1NI6TiAibGCr41R5iTC7cgPPgMUDKlAaGml');
define('SECURE_AUTH_SALT', 'zV9bfsUTIUmBHGaLbjlEkKJoBDDi9E4HcaRCBaSsmsQbCFOPVXUZ6IoxDu5QTtMe');
define('LOGGED_IN_SALT', 'C2Xa67GWAV6ZcBd7brfiA4w0ZU4m3IQcgb39ugJ4qAnq6gXZeMXn1kpHG84GyeZQ');
define('NONCE_SALT', 'UIYDwVjYoT1nzTqubstGAbzLgJoaJXaC6NQ4WA8heU9Fr5NzxGQTyxq9P4v9j5Fh');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
