<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'captured_capwp_w');

/** MySQL database username */
define('DB_USER', 'captured_capwp_w');

/** MySQL database password */
define('DB_PASSWORD', 'fQaOX3ol');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '1kW4BlOzOYzHSueXxhLxGlF7wYcKOpNEeygsf0Vr63GwiERjSci0D7pcXM2UtCmo');
define('SECURE_AUTH_KEY', '306Mdk6Ujdl0UH7tIbvMzrFzahyX05tY6Lj0IVdjjKXks5TigypkMHnuySH1oWhb');
define('LOGGED_IN_KEY', 'fJArVrGLsrh8DMcI8rUsCOqF81BLUFvOKuObhe1lZXtRrP3p4RkZgaglnKMuWmS5');
define('NONCE_KEY', 'wtyRRAHdhlfsB4ClSRPg7FwqEZb9lgoMVS31uHsUTtKA8mxA7cFgGQMZSvihqhLs');
define('AUTH_SALT', 'VigOBGtK6Wc0niWN9jccXsbfVI6X5MfCJAksdmbGK3vd2yxwm329PucMvL7m44cI');
define('SECURE_AUTH_SALT', 'aSVUbDDQniNwGPsAYXHeeK17lYOL3gd5v0EN49rulUqAdQS6fUtE3Sd2rLD8t2AI');
define('LOGGED_IN_SALT', 'ygCt79nXvChPW8EDTowojhmy5Yjjokq9mrUzeWoUGR3SaV0oCM42uxv8hVIhFNsN');
define('NONCE_SALT', 'NesmmET9rOb9aEmgHaeHayWbYsupoKnUogEkKEgVlbQmD8IxFO7JQVJuuWP8Duep');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
