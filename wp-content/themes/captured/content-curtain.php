<?php
/**
 * @package captured
 */
?>
<!--Entrance Curtain-->
<div class="curtain">

	<style>
		video#bgvid {
			position: fixed;
			top: 50%;
			left: 50%;
			min-width: 100%;
			min-height: 100%;
			width: auto;
			height: auto;
			z-index: -100;
			-webkit-transform: translateX(-50%) translateY(-50%);
			transform: translateX(-50%) translateY(-50%);
			/*background: url(polina.jpg) no-repeat;
			background-size: cover; */
		}

		@media screen and (max-device-width: 1024px) {
			#bgvid { display: none; }
		}
	</style>
	<video autoplay  poster="" id="bgvid">
		<source src="/wp-content/themes/captured/video/cover_vid.mp4" type="video/mp4">
	</video>

	<div class="inside-curtain vert-mid">
		<div class="curtain-copy">
			<img class="fiftytwo" src="<?php bloginfo('template_url'); ?>/images/captured52_wordmark_white.svg" alt="52">
			<h2 id="exclusive" class="letter">THE MOST</h2>
            <h2 id="exclusive" class="letter">STRIKING</h2>
			<h2 id="large-format" class="letter">LARGE FORMAT</h2>
			<h2 id="photographs" class="letter">PHOTOGRAPHS</h2>
            <div class="squeeze">
				<h3 id="gone" class="lettering">IN THE WORLD.</h3>
			</div>
			<hr></hr>
		</div>
		<a id="entrance" class="button">LET'S GO!</a>

	</div>

</div>

<!-- Flash -->
<div class="flash">
</div>