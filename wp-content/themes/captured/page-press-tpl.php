<?php
/**
 * Template Name: Press Template
 *
 *
 * @package captured
 */

get_header(); ?>

		<?php while ( have_posts() ) : the_post(); ?>
	

		<div  class="press-contain">

			<?php

			// check if the repeater field has rows of data
			if( have_rows('press_item') ):

			 	// loop through the rows of data
			    while ( have_rows('press_item') ) : the_row(); ?>
				<div class="press-item">
			      
			        		
			        	<a class="press-img-link" href="<?php the_sub_field('press_link'); ?>" target="_blank">
			        		<img class="" src="<?php the_sub_field('press_image'); ?>">
			        	</a>

			       
			       </div>

			   <?php endwhile;

			else :

			    // no rows found

			endif;

?>
		</div>

			<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
