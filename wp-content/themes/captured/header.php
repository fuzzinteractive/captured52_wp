<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package captured
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" type="text/css" href="//cloud.typography.com/6739354/705126/css/fonts.css" />
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<noscript>
    <style type="text/css">
        .site-wrapper {display:none;}
        .curtain {display: none;}
    </style>
    <div class="noscriptmsg" style="text-align: center;">
    <h4>This site requires javascript to be enabled<br>
      You don't have javascript enabled.<br>
      <a href="http://enable-javascript.com/">How to enable javascript</a></h4>
    </div>
</noscript>

  <?php
    //Opening Curtain for Home Page
    if(is_front_page()){ 
      get_template_part( 'content', 'curtain' );
    }
  ?>


  <div class="site-wrapper">
    <a id="nav-toggle" href="#"><span></span></a>
    <div id="shopp-cart-ajax">
          <?php if ( shopp( 'cart.hasitems' ) ) : ?>

            <a href="<?php shopp( 'cart.url' ); ?>"><span class="cart-header">k</span> (<?php shopp( 'cart.total-quantity' ); ?>)</a>

          <?php else : ?>

          <?php endif; ?>
      </div>

    <div class="header" <?php if(is_front_page()){} else if(is_page(33)) { echo 'data-ix="hide-nav"'; } else if(!is_single()) { echo 'data-ix="slide-in-on-load"';} else {} ?>>
       
      <div class="w-clearfix navbar">
        <div class="brand">
          <a class="w-inline-block" href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url') ?>/images/captured52_wordmark.svg" width="180" alt="54ada65d9db759b733058a8c_Captured_logo.png">
          </a>
        </div>
      </div>
    </div>

    <div class="nav-open">
      <div class="vert-mid">
        <?php wp_nav_menu( array('menu' => 'Primary Nav' )); ?>
        <div class="sub-nav">
          <div>
            <?php wp_nav_menu( array('menu' => 'Sub Nav' )); ?>
          </div>
        </div>
      </div>
    </div>