<?php
/**
 * Template Name: Value Template
 *
 *
 * @package captured
 */

get_header(); ?>

			<?php while ( have_posts() ) : the_post(); ?>

		  <div class="artists-back">
		    <div class="w-container copy-contain">
		      <div class="white-copy-contain push">
		        <h4 data-ix="fade-in-from-left">no boundaries</h4>
		        <p class="iceburg-copy" data-ix="fade-in-from-left">Captured52&nbsp;is common ground — rooted in the concept of collective participation among our photographers and those who support them.&nbsp;&nbsp;
		          <br>
		          <br>This is our community, built on the uncompromising pursuit of the most striking large format photographs in the world — strong today, stronger in the future.&nbsp;&nbsp;
		          <br>
		          <br>To ensure integrity is not compromised for the sake of sales, all 52 photographers share equally — across all 52 images at the end of the season.&nbsp;
		          <br>
		          <br>Unconventional? Yes. But this is Captured52 — the art of collaboration.</p>
		      </div>
		    </div>
		  </div>

			<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
