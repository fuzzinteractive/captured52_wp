<?php
/**
 * Template Name: TOC Template
 *
 *
 * @package captured
 */

get_header(); ?>

			<?php while ( have_posts() ) : the_post(); ?>
				<div>
			    <div class="south"></div>
			    <div class="west"></div>
			    <div class="east"></div>
			    <div class="north"></div>
			  </div>
			  <div class="terms">
			    <div class="w-container terms-copy">
			      <div class="white-copy-contain">
			        <h4 class="dark">terms &amp; conditions</h4>
			        <!--<p class="terms-text">We'd like to just say, "Do the right thing," but alas, the world is a complicated place and thus, we must embellish.
			          <br>
			          <br>If you continue to browse and use captured52.com (Website), you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern Captured, LLC and our relationship with you in relation to this Website. If you disagree with any part of these Terms and Conditions, please do not use our website.
			          <br>
			          <br>The term 'Captured, LLC' or 'us' or 'we' refers to the owner of the Website whose registered office is 4905 SW Hewett Blvd, Portland, OR 97221. The term 'you' refers to the user or viewer of our Website.The use of this Website is subject to the following terms of use:
			          <br>The content of the pages of this Website is for your general information and use only. It is subject to change without notice.
			          <br>This Website uses cookies to monitor browsing preferences. If you do allow cookies to be used, the following personal information may be stored by us for use by third parties: name, address, phone, email, age and/or gender.
			          <br>
			          <br>Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this Website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.
			          <br>
			          <br>Your use of any information or materials on this Website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this Website meet your specific requirements.
			          <br>
			          <br>This Website contains material which is owned by us and/or the photographer represented on this Website. This material includes, but is not limited to photographs, images, design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with U.S. copyright law and all content on this Website is covered by U.S and International copyright law.
			          <br>All trade marks reproduced in this Website which are not the property of, or licensed to, the operator are acknowledged on the Website.
			          <br>
			          <br>Unauthorized use of this Website may give rise to a claim for damages and/or be a criminal offense.
			          <br>From time to time this Website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).
			          <br>
			          <br>Your use of this website and any dispute arising out of such use of the website is subject to the laws of the United States of America.
			          <br>
			          <br><em>DISCLAIMER OF WARRANTIES<br xmlns="http://www.w3.org/1999/xhtml"></em>
			          <br>YOU UNDERSTAND AND AGREE THAT:
			          <br>THE SITE, INCLUDING, WITHOUT LIMITATION, ALL CONTENT, FUNCTION, MATERIALS AND SERVICES IS PROVIDED "AS IS," WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, ANY WARRANTY FOR INFORMATION, DATA, DATA PROCESSING SERVICES OR UNINTERRUPTED ACCESS, ANY WARRANTIES CONCERNING THE AVAILABILITY, ACCURACY, COMPLETENESS, USEFULNESS, OR CONTENT OF INFORMATION, AND ANY WARRANTIES OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. WE DO NOT WARRANT THAT THE SITE OR THE FUNCTION, CONTENT OR SERVICES MADE AVAILABLE THEREBY WILL BE TIMELY, SECURE, UNINTERRUPTED OR ERROR FREE, OR THAT DEFECTS WILL BE CORRECTED. WE MAKE NO WARRANTY THAT THE SITE WILL MEET USERS' EXPECTATIONS OR REQUIREMENTS. NO ADVICE, RESULTS OR INFORMATION, OR MATERIALS WHETHER ORAL OR WRITTEN, OBTAINED BY YOU THROUGH THE SITE SHALL CREATE ANY WARRANTY NOT EXPRESSLY MADE HEREIN. IF YOU ARE DISSATISFIED WITH THE SITE, YOUR SOLE REMEDY IS TO DISCONTINUE USING THE SITE.
			          <br>
			          <br>ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE SITE IS DONE AT YOUR OWN DISCRETION AND RISK AND THAT YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH MATERIAL.
			          <br>
			          <br>WE DO NOT ENDORSE, WARRANT OR GUARANTEE ANY PRODUCTS OR SERVICES OFFERED OR PROVIDED BY OR ON BEHALF OF THIRD PARTIES ON OR THROUGH THE SITE. WE ARE NOT A PARTY TO, AND DO NOT MONITOR, ANY TRANSACTION BETWEEN USERS AND THIRD PARTIES WITHOUT THE DIRECT INVOLVEMENT OF CAPTURED, LLC.
			          <br>
			          <br><em>LIMITATION OF LIABILITY<br xmlns="http://www.w3.org/1999/xhtml"></em>
			          <br>IN NO EVENT SHALL CAPTURED, LLC, ITS AFFILIATES OR ANY OF THEIR RESPECTIVE DIRECTORS, OFFICERS, EMPLOYEES, AGENTS, PARTNERS, SUBSIDIARIES, DIVISIONS, SUCCESSORS, SUPPLIERS, DISTRIBUTORS, AFFILIATES VENDORS, CONTRACTORS, GALLERIES, ARTISTS, INSTITUTIONS, REPRESENTATIVES OR CONTENT OR SERVICE PROVIDERS BE LIABLE FOR ANY INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL, EXEMPLARY OR PUNITIVE DAMAGES ARISING FROM OR DIRECTLY OR INDIRECTLY RELATED TO THE USE OF, OR THE INABILITY TO USE, THE SITE OR THE CONTENT, MATERIALS AND FUNCTION RELATED THERETO, INCLUDING, WITHOUT LIMITATION, LOSS OF REVENUE, OR ANTICIPATED PROFITS, OR LOST BUSINESS, DATA OR SALES, OR COST OF SUBSTITUTE SERVICES, EVEN IF CAPTURED, LLC OR ITS REPRESENTATIVE OR SUCH INDIVIDUAL HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE LIMITATION OR EXCLUSION OF LIABILITY SO SOME OF THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU. IN NO EVENT SHALL THE TOTAL LIABILITY OF CAPTURED, LLC TO YOU FOR ALL DAMAGES, LOSSES, AND CAUSES OF ACTION (WHETHER IN CONTRACT OR TORT, INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE OR OTHERWISE) ARISING FROM THE TERMS OR YOUR USE OF THE SITE EXCEED, IN THE AGGREGATE, $100.00. WITHOUT LIMITING THE FOREGOING, IN NO EVENT SHALL CLCV,INC OR ITS RESPECTIVE OFFICERS DIRECTORS, EMPLOYEES, AGENTS, SUCCESSORS, SUBSIDIARIES, DIVISIONS, DISTRIBUTORS, SUPPLIERS, AFFILIATES OR THIRD PARTIES PROVIDING INFORMATION ON THIS SITE HAVE ANY LIABILITY FOR ANY DAMAGES OR LOSSES ARISING OUT OF OR OTHERWISE INCURRED IN CONNECTION WITH THE LOSS OF ANY DATA OR INFORMATION CONTAINED IN YOUR ACCOUNT OR OTHERWISE STORED BY OR ON BEHALF CAPTURED, LLC.
			          <br>
			          <br>You hereby acknowledge that the preceding paragraph shall apply to all content, merchandise and services available through the Website.
			          <br>
			          <br>OREGON LAW. Oregon law governs these Terms regardless of conflict-of-law principles.
			          <br>
			          <br><strong>Website Access<br xmlns="http://www.w3.org/1999/xhtml"></strong>Captured, LLC hereby grants you permission to use the Website as set forth in these Terms and Conditions, provided that: (i) your use of the Website as permitted is solely for your personal, noncommercial use; (ii) you will not copy or distribute any part of the Website in any medium without the prior written authorization of Captured, LLC; (iii) you will not alter or modify any part of the Website other than as may be reasonably necessary to use the Website for its intended purpose; and (iv) you will otherwise comply with the terms and conditions of these Terms of Use.
			          <br>
			          <br>In order to access some features of the Website, you will have to create an account. You are not allowed to use another users account without permission. When creating your account, you must provide accurate and complete information. You are solely responsible for the activity that occurs on your account, and you must keep your account password secure. You may change your password at any time by updating your Account page. In addition, you agree to immediately notify Captured, LLC of any unauthorized use of your password or account or any other breach of security. Captured, LLC cannot and will not be liable for any loss or damage arising from your failure to comply with these Terms and Conditions. It is expressly understood that Captured, LLC retains access to Artist Account pages to approve or disapprove images, push information live, edit images, size, price, paper information, make corrections and/or revisions as mutually agreed upon by Artist and Captured, LLC.
			          <br>
			          <br>By registering with Captured, LLC, you represent that you are of legal age to form a binding contract and are not a person barred by any laws from using the captured52.com Website. You agree to provide true, accurate, current and complete information about yourself in all required fields of the registration form. If any of your information changes, you agree to update your registration information as soon as possible. If Captured, LLC suspects that your registration information is not complete, current, or accurate, or that you have otherwise violated these Terms and Conditions, your account may be subject to suspension or termination, and you may be barred from using the captured52.com Website.
			          <br>
			          <br>You agree not to use or launch any automated system, including without limitation, "robots," "spiders," or similar technological devices or programs, that access the Website in a manner that sends more request messages to the captured52.com servers in a given period of time than a human can reasonably produce in the same period by using a conventional on-line web browser. Notwithstanding the foregoing, Captured, LLC grants the operators of public search engines permission to use spiders to copy materials from the Website for the sole purpose of creating publicly available searchable indices of the materials, but not caches or archives of such materials. Captured, LLC reserves the right to revoke these exceptions either generally or in specific cases, in its sole discretion.
			          <br>
			          <br>You agree not to collect or use any personally identifiable information ("Personal Information") including without limitation account names, email addresses, or other User Submissions (as defined below), from the Website, nor use the communication systems provided by the Website for any commercial solicitation purposes, including without limitation to solicit, for commercial purposes, any users of the Website.
			          <br>
			          <br><strong>User Submitted Content<br xmlns="http://www.w3.org/1999/xhtml"></strong>You are responsible for any User Content you post to the site. By "User Content" we mean any content you post to the site, which may include reviews, comments, image uploading, captions, participating in forums and other such features that allow you to add content to the site. We are not responsible for the personally identifiable or other information you choose to submit as User Content and we reserve the right to remove any User Content generated by any user at our sole discretion. You understand that once you post User Content, your content becomes public. We are not responsible for keeping any User Content confidential so if you do not want anyone to read or see that content, do not submit or post it to the Site.
			          <br>
			          <br>If we allow you to upload User Content, you may not:
			          <br>provide User Content that you do not have the right to submit, unless you have the owner's permission; this includes material covered by someone else's copyright, patent, trade secret, privacy, publicity, or any other proprietary right;
			          <br>forge headers or manipulate other identifiers in order to disguise the origin of any User Content you provide;
			          <br>provide any User Content that contains lies, falsehoods or misrepresentations that could damage us or anyone else;
			          <br>provide User Content that is illegal, obscene, defamatory, libelous, threatening, pornographic, harassing, hateful, racially or ethnically offensive, or encourage conduct that would be considered a criminal offense, give rise to civil liability, violate any law, or is otherwise inappropriate;
			          <br>
			          <br>impersonate anyone else or lie about your affiliation with another person or entity in your User Content;
			          <br>use meta tags or any other "hidden text" utilizing any of our or our suppliers' product names or trademarks in your User Content; or
			          <br>provide User Content which disparage us or our vendors, partners, contractors, galleries, artists, institutions, distributers, representatives and affiliates.
			          <br>
			          <br>Except as otherwise specifically provided, if you post content or submit material to the Site, you grant us a nonexclusive, royalty-free, perpetual, irrevocable, and fully sub-licensable right to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute, and display such content throughout the world in any media. You represent and warrant that you own or otherwise control all of the rights to the content that you post; that the content is accurate; that use of the content you supply does not violate these Terms or any law or regulation; and the content will not cause injury to any person or entity. We have the right but not the obligation to monitor and edit or remove any activity or content. User Content comes from a variety of sources. We do not endorse, or support any views, opinions, recommendations, or advice that may be in User Content, nor do we vouch for its accuracy or its reliability, usefulness, safety or intellectual property rights of any User Content. We take no responsibility and assume no liability for any User Content posted by you or any third party.
			          <br>
			          <br><strong>International Use<br xmlns="http://www.w3.org/1999/xhtml"></strong>We control and operate the Site from our offices in the United States of America, and all information is processed within the United States. We do not represent that materials on the Site are appropriate or available for use in other locations. Persons who choose to access the Site from other locations do so on their own initiative, and are responsible for compliance with local laws, if and to the extent local laws are applicable.
			          <br>
			          <br>You agree to comply with all applicable laws, rules and regulations in connection with your use of the Site. Without limiting the generality of the foregoing, you agree to comply with all applicable laws regarding the transmission of technical data exported from the United States or the country in which you reside.
			          <br>
			          <br><strong>Our Privacy Policy<br xmlns="http://www.w3.org/1999/xhtml"></strong>This privacy policy sets out how Captured, LLC uses and protects any information that you give Captured, LLC when you use this Website.
			          <br>
			          <br>Captured, LLC is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this Website, then you can be assured that it will only be used in accordance with this privacy statement.
			          <br>
			          <br>Captured, LLC may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes. This policy is effective from February 1, 2015.
			          <br>
			          <br>What we collect
			          <br>We may collect the following information:
			          <br>
			          <br>name and location
			          <br>contact information including email address
			          <br>demographic information such as zip code/postcode, preferences and interests
			          <br>other information relevant to customer surveys and/or offers
			          <br>
			          <br>What we do with the information we gather
			          <br>We require this information to understand your needs and provide you with a better service, and in particular for the following reasons:
			          <br>
			          <br>Internal record keeping.
			          <br>We may use the information to improve our products and services.
			          <br>We may periodically send promotional emails about new products, special offers or other information which we think you may find interesting using the email address which you have provided.
			          <br>From time to time, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail. We may use the information to customize the website according to your interests.
			          <br>
			          <br>Security
			          <br>We are committed to ensuring that your information is secure. In order to prevent unauthorized access or disclosure, we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.
			          <br>
			          <br>How we use cookies
			          <br>A cookie is a small file which asks permission to be placed on your computer's hard drive. Once you agree, the file is added and the cookie helps analyze web traffic or lets you know when you visit a particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences.
			          <br>
			          <br>We use traffic log cookies to identify which pages are being used. This helps us analyze data about webpage traffic and improve our Website in order to tailor it to customer needs. We only use this information for statistical analysis purposes and then the data is removed from the system.
			          <br>
			          <br>Overall, cookies help us provide you with a better website by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us.
			          <br>
			          <br>You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. This may prevent you from taking full advantage of the website.
			          <br>
			          <br>Links to other websites
			          <br>Our Website may contain links to other websites of interest. We have no control over the nature, content and availability of those sites. The inclusion of any links does not necessarily imply a recommendation or endorse the views expressed within them. However, once you have used these links to leave our Website, you should note that we do not have any control over that other website. Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst visiting such sites and such sites are not governed by this privacy statement. You should exercise caution and look at the privacy statement applicable to the website in question.
			          <br>
			          <br>Controlling your personal information
			          <br>You may choose to restrict the collection or use of your personal information in the following ways:&nbsp;
			          <br>
			          <br>whenever you are asked to fill in a form on the Website, look for the box that you can click to indicate that you do not want the information to be used by anybody for direct marketing purposes&nbsp;
			          <br>if you have previously agreed to us using your personal information for direct marketing purposes, you may change your mind at any time by writing to or emailing us at service@captured52.com
			          <br>
			          <br>We will not sell, distribute or lease your personal information to third parties unless we have your permission or are required by law to do so. We may use your personal information to send you promotional information about third parties which we think you may find interesting if you tell us that you wish this to happen.
			          <br>
			          <br>If you believe that any information we are holding on you is incorrect or incomplete, please write to or email us as soon as possible at the above address. We will promptly correct any information found to be incorrect.
			          <br>
			          <br>Website Disclaimer
			          <br>The information contained in this Website is for general information purposes only. The information is provided by Captured, LLC and while we endeavor to keep the information up to date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the Website or the information, products, services, or related graphics contained on the Website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.
			          <br>
			          <br>In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of, or in connection with, the use of this Website.
			          <br>
			          <br>Every effort is made to keep the Website up and running smoothly. However, Captured, LLC takes no responsibility for, and will not be liable for, the Website being temporarily unavailable due to technical issues beyond our control.
			          <br>
			          <br>Consent to Processing
			          <br>By providing any personal information to the Site, all users, including without limitation users in the European Union, fully understand and unambiguously consent to the collection and processing of such information in the United States. Any inquiries concerning these Terms should be directed to us at the address below.
			          <br>
			          <br>Risk of Loss
			          <br>The items purchased from our Site are shipped by a third-party carrier pursuant to a shipment content. As a result, risk of loss and title for such items may pass to you upon our delivery to the carrier.
			          <br>
			          <br>Purchasing
			          <br>Captured, LLC and its partners strive for complete accuracy in description and pricing of the products on the Website. However, due to the nature of the internet, occasional glitches, service interruptions or mistakes may cause inaccuracies to appear on the Website. Captured, LLC has the right to void any purchases that display an inaccurate price. If the displayed price is higher than the actual price, you may be refunded the overcharge. If the displayed price is less than the actual price, Captured, LLC will void the purchase and attempt to contact you via either phone or email to inquire if you would like the item for the correct price.
			          <br>
			          <br>You acknowledge that temporary interruptions in the availability of the Site may occur from time to time as normal events. Also, we may decide to cease making available the Website or any portion of the Website at any time and for any reason. Under no circumstances will Captured, LLC or its suppliers be held liable for any damages due to such interruptions or lack of availability.
			          <br>
			          <br>Notices
			          <br>Notices to you may be made via either email or regular mail. The Website may also provide notices of changes to the Terms and Conditions or other matters by displaying notices or links to notices to you on the Website.
			          <br>
			          <br>Contacting Us
			          <br>To contact us with any questions or concerns in connection with this Agreement or the Website, or to provide any notice under this Agreement to us please go to Contact Us or write to us at:
			          <br>
			          <br>Captured, LLC
			          <br>4905 SW Hewett Blvd
			          <br>Portland, OR 97221
			          <br>Fax: 503.292.8101</p>-->



			          <p><strong>We'd like to just say, "Do the right thing," but alas, the world is a complicated place and thus, we must embellish.</strong></p>
			          <p><strong>Terms and Conditions</strong></p>
			          <p>If you continue to browse and use captured52.com (Website), you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern Captured, LLC and our relationship with you in relation to this Website. If you disagree with any part of these Terms and Conditions, please do not use our Website.</p>
			          <p>The term 'Captured, LLC' or 'us' or 'we' refers to the owner of the Website whose registered office is 4905 SW Hewett Blvd, Portland, OR 97221. The term 'you' refers to the user or viewer of our Website.The use of this Website is subject to the following terms of use:</p>
			          <ul>
			          	<li>The content of the pages of this Website is for your general information and use only. It is subject to change without notice.</li>
			          	<li>The Website features photographs, whose integrity we do not compromise and therefore may be deemed offensive by some.</li>
			          	<li>This Website uses cookies to monitor browsing preferences. If you do allow cookies to be used, the following personal information may be stored by us for use by third parties: name, address, phone, email, age and/or gender.</li>
			          	<li>Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this Website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</li>
			          	<li>Your use of any information or materials on this Website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this Website meet your specific requirements.</li>
			          	<li>This Website contains material which is owned by us and/or the photographers represented on this Website. This material includes, but is not limited to photographs, images, design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with U.S. copyright law and all content on this Website is covered by U.S and International copyright law.</li>
			          	<li>All trademarks reproduced in this Website which are not the property of, or licensed to, the operator are acknowledged on the Website.</li>
			          	<li>Unauthorized use of this Website may give rise to a claim for damages and/or be a criminal offense.</li>
			          	<li>From time to time this Website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).</li>
			          	<li>Your use of this Website and any dispute arising out of such use of the Website is subject to the laws of the United States of America.</li>
			          </ul>

			          <p><strong>DISCLAIMER OF WARRANTIES</strong></p>
			          <p>YOU UNDERSTAND AND AGREE THAT:</p>
			          <p>THE WEBSITE, INCLUDING, WITHOUT LIMITATION, ALL CONTENT, FUNCTION, MATERIALS AND SERVICES IS PROVIDED "AS IS," WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, ANY WARRANTY FOR INFORMATION, DATA, DATA PROCESSING SERVICES OR UNINTERRUPTED ACCESS, ANY WARRANTIES CONCERNING THE AVAILABILITY, ACCURACY, COMPLETENESS, USEFULNESS, OR CONTENT OF INFORMATION, AND ANY WARRANTIES OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. WE DO NOT WARRANT THAT THE WEBSITE OR THE FUNCTION, CONTENT OR SERVICES MADE AVAILABLE THEREBY WILL BE TIMELY, SECURE, UNINTERRUPTED OR ERROR FREE, OR THAT DEFECTS WILL BE CORRECTED. WE MAKE NO WARRANTY THAT THE WEBSITE WILL MEET USERS' EXPECTATIONS OR REQUIREMENTS. NO ADVICE, RESULTS OR INFORMATION, OR MATERIALS WHETHER ORAL OR WRITTEN, OBTAINED BY YOU THROUGH THE WEBSITE SHALL CREATE ANY WARRANTY NOT EXPRESSLY MADE HEREIN. IF YOU ARE DISSATISFIED WITH THE WEBSITE, YOUR SOLE REMEDY IS TO DISCONTINUE USING THE WEBSITE.</p>
			          <p>ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE WEBSITE IS DONE AT YOUR OWN DISCRETION AND RISK AND THAT YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH MATERIAL.</p>
			          <p>WE DO NOT ENDORSE, WARRANT OR GUARANTEE ANY PRODUCTS OR SERVICES OFFERED OR PROVIDED BY OR ON BEHALF OF THIRD PARTIES ON OR THROUGH THE WEBSITE. WE ARE NOT A PARTY TO, AND DO NOT MONITOR, ANY TRANSACTION BETWEEN USERS AND THIRD PARTIES WITHOUT THE DIRECT INVOLVEMENT OF CAPTURED, LLC.</p>
			          <p>LIMITATION OF LIABILITY</p>
			          <p>IN NO EVENT SHALL CAPTURED, LLC, ITS AFFILIATES OR ANY OF THEIR RESPECTIVE DIRECTORS, OFFICERS, EMPLOYEES, AGENTS, PARTNERS, SUBSIDIARIES, DIVISIONS, SUCCESSORS, SUPPLIERS, DISTRIBUTORS, AFFILIATES VENDORS, CONTRACTORS, GALLERIES, ARTISTS, INSTITUTIONS, REPRESENTATIVES OR CONTENT OR SERVICE PROVIDERS BE LIABLE FOR ANY INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL, EXEMPLARY OR PUNITIVE DAMAGES ARISING FROM OR DIRECTLY OR INDIRECTLY RELATED TO THE USE OF, OR THE INABILITY TO USE, THE WEBSITE OR THE CONTENT, MATERIALS AND FUNCTION RELATED THERETO, INCLUDING, WITHOUT LIMITATION, LOSS OF REVENUE, OR ANTICIPATED PROFITS, OR LOST BUSINESS, DATA OR SALES, OR COST OF SUBSTITUTE SERVICES, EVEN IF CAPTURED, LLC OR ITS REPRESENTATIVE OR SUCH INDIVIDUAL HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE LIMITATION OR EXCLUSION OF LIABILITY SO SOME OF THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU. IN NO EVENT SHALL THE TOTAL LIABILITY OF CAPTURED, LLC TO YOU FOR ALL DAMAGES, LOSSES, AND CAUSES OF ACTION (WHETHER IN CONTRACT OR TORT, INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE OR OTHERWISE) ARISING FROM THE TERMS OR YOUR USE OF THE WEBSITE EXCEED, IN THE AGGREGATE, $100.00. WITHOUT LIMITING THE FOREGOING, IN NO EVENT SHALL CAPTURED, LLC OR ITS RESPECTIVE OFFICERS DIRECTORS, EMPLOYEES, AGENTS, SUCCESSORS, SUBSIDIARIES, DIVISIONS, DISTRIBUTORS, SUPPLIERS, AFFILIATES OR THIRD PARTIES PROVIDING INFORMATION ON THIS SITE HAVE ANY LIABILITY FOR ANY DAMAGES OR LOSSES ARISING OUT OF OR OTHERWISE INCURRED IN CONNECTION WITH THE LOSS OF ANY DATA OR INFORMATION CONTAINED IN YOUR ACCOUNT OR OTHERWISE STORED BY OR ON BEHALF OF CAPTURED, LLC.</p>
			          <p>You hereby acknowledge that the preceding paragraph shall apply to all content, merchandise and services available through the Website.</p>
			          <p>OREGON LAW. Oregon law governs these Terms regardless of conflict-of-law principles.</p>
			          <p><strong>Website Access</strong><br>Captured, LLC hereby grants you permission to use the Website as set forth in these Terms and Conditions, provided that: (i) your use of the Website as permitted is solely for your personal, noncommercial use; (ii) you will not copy or distribute any part of the Website in any medium without the prior written authorization of Captured, LLC; (iii) you will not alter or modify any part of the Website other than as may be reasonably necessary to use the Website for its intended purpose; and (iv) you will otherwise comply with the terms and conditions of these Terms of Use.</p>
			          <p>In order to access some features of the Website, you will have to create an account. You are not allowed to use another users account without permission. When creating your account, you must provide accurate and complete information. You are solely responsible for the activity that occurs on your account, and you must keep your account password secure. You may change your password at any time by updating your Account page. In addition, you agree to immediately notify Captured, LLC of any unauthorized use of your password or account or any other breach of security. Captured, LLC cannot and will not be liable for any loss or damage arising from your failure to comply with these Terms and Conditions. It is expressly understood that Captured, LLC retains access to Artist Account pages to approve or disapprove images, push information live, edit images, size, price, paper or frame information, make corrections and/or revisions at the sole discretion of Captured, LLC.</p>
			          <p>By registering with Captured, LLC, you represent that you are of legal age to form a binding contract and are not a person barred by any laws from using the Website. You agree to provide true, accurate, current and complete information about yourself in all required fields of the registration form. If any of your information changes, you agree to update your registration information as soon as possible. If Captured, LLC suspects that your registration information is not complete, current, or accurate, or that you have otherwise violated these Terms and Conditions, your account may be subject to suspension or termination, and you may be barred from using the Website. </p>
			          <p>You agree not to use or launch any automated system, including without limitation, "robots," "spiders," or similar technological devices or programs, that access the Website in a manner that sends more request messages to the Website servers in a given period of time than a human can reasonably produce in the same period by using a conventional on-line web browser. Notwithstanding the foregoing, Captured, LLC grants the operators of public search engines permission to use spiders to copy materials from the Website for the sole purpose of creating publicly available searchable indices of the materials, but not caches or archives of such materials. Captured, LLC reserves the right to revoke these exceptions either generally or in specific cases, in its sole discretion. </p>
			          <p>You agree not to collect or use any personally identifiable information ("Personal Information") including without limitation account names, email addresses, or other User Submissions (as defined below), from the Website, nor use the communication systems provided by the Website for any commercial solicitation purposes, including without limitation to solicit, for commercial purposes, any users of the Website. </p>
			          <p><strong>User Submitted Content</strong></p>
			          <p>You are responsible for any User Content you post to the site. By "User Content" we mean any content you post to the site, which may include reviews, comments, image uploading, captions, participating in forums and other such features that allow you to add content to the site. We are not responsible for the personally identifiable or other information you choose to submit as User Content and we reserve the right to remove any User Content generated by any user at our sole discretion. You understand that once you post User Content, your content becomes public. We are not responsible for keeping any User Content confidential so if you do not want anyone to read or see that content, do not submit or post it to the Site.</p>
			          <p>If we allow you to upload User Content, you may not:</p>
			          <ul>
			          	<li>provide User Content that you do not have the right to submit, unless you have the owner's permission; this includes material covered by someone else's copyright, patent, trade secret, privacy, publicity, or any other proprietary right;</li>
			          	<li>forge headers or manipulate other identifiers in order to disguise the origin of any User Content you provide;</li>
			          	<li>provide any User Content that contains lies, falsehoods or misrepresentations that could damage us or anyone else;</li>
			          	<li>provide User Content that is illegal, obscene, defamatory, libelous, threatening, pornographic, harassing, hateful, racially or ethnically offensive, or encourage conduct that would be considered a criminal offense, give rise to civil liability, violate any law, or is otherwise inappropriate;</li>
			          	<li>impersonate anyone else or lie about your affiliation with another person or entity in your User Content;</li>
			          	<li>use meta tags or any other "hidden text" utilizing any of our or our suppliers' product names or trademarks in your User Content; or</li>
			          	<li>provide User Content which disparage us or our vendors, partners, contractors, galleries, artists, institutions, distributers, representatives and affiliates.</li>
			          </ul>
			          <p>Except as otherwise specifically provided, if you post content or submit material to the Website, you grant us a nonexclusive, royalty-free, perpetual, irrevocable, and fully sub-licensable right to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute, and display such content throughout the world in any media. You represent and warrant that you own or otherwise control all of the rights to the content that you post; that the content is accurate; that use of the content you supply does not violate these Terms or any law or regulation; and the content will not cause injury to any person or entity. We have the right but not the obligation to monitor and edit or remove any activity or content. User Content comes from a variety of sources. We do not endorse, or support any views, opinions, recommendations, or advice that may be in User Content, nor do we vouch for its accuracy or its reliability, usefulness, safety or intellectual property rights of any User Content. We take no responsibility and assume no liability for any User Content posted by you or any third party. </p>
			          <p><strong>International Use</strong></p>
			          <p>We control and operate the Website from our offices in the United States of America, and all information is processed within the United States. We do not represent that materials on the Website are appropriate or available for use in other locations. Persons who choose to access the Website from other locations do so on their own initiative, and are responsible for compliance with local laws, if and to the extent local laws are applicable.</p>
			          <p>You agree to comply with all applicable laws, rules and regulations in connection with your use of the Website. Without limiting the generality of the foregoing, you agree to comply with all applicable laws regarding the transmission of technical data exported from the United States or the country in which you reside. </p>
			          <p><strong>Our Privacy Policy</strong></p>
			          <p>This privacy policy sets out how Captured, LLC uses and protects any information that you give Captured, LLC when you use this Website.</p>
			          <p>Captured, LLC is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this Website, then you can be assured that it will only be used in accordance with this privacy statement.</p>
			          <p>Captured, LLC may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes. This policy is effective from May 2, 2015.</p>
			          <p><strong>What we collect</strong><br>We may collect the following information:</p>
			          <ul>
			          	<li>name and location</li>
						<li>contact information including email address</li>
						<li>demographic information such as zip code/postcode, preferences and interests</li>
						<li>other information relevant to customer surveys and/or offers</li>
			          </ul>
			          <p>What we do with the information we gather. We require this information to understand your needs and provide you with a better service, and in particular for the following reasons:</p>
			          <ul>
			          	<li>Internal record keeping.</li>
						<li>We may use the information to improve our products and services.</li>
						<li>We may periodically send promotional emails about new products, special offers or other information which we think you may find interesting using the email address which you have provided.</li>
						<li>From time to time, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail. We may use the information to customize the Website according to your interests.</li>
			          </ul>
			          <p><strong>Security</strong><br>
					  We are committed to ensuring that your information is secure. In order to prevent unauthorized access or disclosure, we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.</p>
					  <p><strong>How we use cookies</strong><br>
						A cookie is a small file which asks permission to be placed on your computer's hard drive. Once you agree, the file is added and the cookie helps analyze web traffic or lets you know when you visit a particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences.</p>
					  <p>We use traffic log cookies to identify which pages are being used. This helps us analyze data about webpage traffic and improve our Website in order to tailor it to customer needs. We only use this information for statistical analysis purposes and then the data is removed from the system.</p>
					  <p>Overall, cookies help us provide you with a better Website by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us.</p>
					  <p>You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. This may prevent you from taking full advantage of the Website.</p>
					  <p><strong>Controlling your personal information</strong><br>
                       You may choose to restrict the collection or use of your personal information in the following ways: </p>
                      <ul>
                      	<li>whenever you are asked to fill in a form on the Website, look for the box that you can click to indicate that you do not want the information to be used by anybody for direct marketing purposes </li>
                      	<li>if you have previously agreed to us using your personal information for direct marketing purposes, you may change your mind at any time by writing to or emailing us at <a href="mailto:service@captured52.com">service@captured52.com</a></li>
                      </ul>
                      <p>We will not sell, distribute or lease your personal information to third parties unless we have your permission or are required by law to do so. We may use your personal information to send you promotional information about third parties which we think you may find interesting if you tell us that you wish this to happen.</p>

                      <p>If you believe that any information we are holding on you is incorrect or incomplete, please write to or email us as soon as possible at the above address. We will promptly correct any information found to be incorrect. </p>
                      <p><strong>Consent to Processing</strong><br>
						By providing any personal information to the Site, all users, including without limitation users in the European Union, UK and the rest of the world, fully understand and unambiguously consent to the collection and processing of such information in the United States. Any inquiries concerning these Terms should be directed to us at the address below.</p>

					  <p><strong>Risk of Loss</strong><br>
						The items purchased from our Website are shipped by a third-party carrier pursuant to a shipment content. As a result, risk of loss and title for such items may pass to you upon our delivery to the carrier.</p>
					  <p><strong>Purchasing</strong><br>
						Captured, LLC and its partners strive for complete accuracy in description and pricing of the products on the Website. However, due to the nature of the internet, occasional glitches, service interruptions or mistakes may cause inaccuracies to appear on the Website. Captured, LLC has the right to void any purchases that display an inaccurate price. If the displayed price is higher than the actual price, you may be refunded the overcharge. If the displayed price is less than the actual price, Captured, LLC will void the purchase and attempt to contact you via either phone or email to inquire if you would like the item for the correct price.</p>
					  <p>You acknowledge that temporary interruptions in the availability of the Website may occur from time to time as normal events. Also, we may decide to cease making available the Website or any portion of the Website at any time and for any reason. Under no circumstances will Captured, LLC or its suppliers be held liable for any damages due to such interruptions or lack of availability.</p>
					  <p><strong>Notices</strong><br>
						Notices to you may be made via either email or regular mail. The Website may also provide notices of changes to the Terms and Conditions or other matters by displaying notices or links to notices to you on the Website.</p>
					  <p><strong>Contacting Us</strong><br>
						To contact us with any questions or concerns in connection with this Agreement or the Website, or to provide any notice under this Agreement to us please go to Contact Us or write to us at:</p>
					  <p>Captured, LLC<br>
						4905 SW Hewett Blvd<br>
						Portland, OR 97221</p>
			      </div>
			    </div>
			  </div>

			<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
