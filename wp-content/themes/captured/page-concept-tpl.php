<?php
/**
 * Template Name: Concept Template
 *
 *
 * @package captured
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>


	<div class="power fixed med-pad">
		<div class ="section-title">
			<h4 data-ix="fade-in-from-right">20" x 24" is an ice cube.<br>
			<strong class="important">60" x 80" is an iceberg.</strong></h4>
		</div>
		<div class="down-btn-white"><a href="#hello" class="icon">o</a></div>		
	</div>


	<div class="message-block" id="hello">
		<div data-ix="fade-in-from-left" class="message-inside">
			<p>The right photograph, when viewed on a grand scale — completely transforms the experience. Every week, we publish an open edition, framed, original photograph in one size - between 40” x 40” and 60" x 80.” From one visionary photographer.</p>
			<p>Big. Powerful. Contemporary Art.</p>
			<p>With each of 52 invited photographers worldwide, we select one, exclusive and breakthrough image whose very essence gives rise to the power of scale. Nothing short of captivating.</p>
		</div>
	</div>



	<div class="insitue scroll med-pad">
		<div class="full-tile-msg vert-mid">
			<h1 data-ix="fade-up">THIS IS LIFE.</h1>
			<h4 data-ix="fade-up">BEAUTIFULLY COMPLEX.</h4>
		</div>
	</div>


	<div class="words scroll">
		<div class="half-screen half-left">
		</div>
		<div class="half-screen half-right">
			<div class="vert-mid">
				<div class="tight-section">
					<div class="tight-header">
						<div class="style-header excel-type" data-ix="fade-in-from-right">EXCELLENCE</div>
						<h4 class="black-type" data-ix="fade-in-from-right">IS ART.</h4>
					</div>
					<div data-ix="fade-in-from-right" class="tight-copy">
						<p>A Captured52 image guarantees you the finest original photograph, craftsmanship and materials. Our printing process is exactly the same as what many of the world’s leading photographers and galleries depend on for museum-ready “limited editions.”</p>
						<p>We print exclusively on Hahnemühle 100% Cotton Photo Rag paper - the choice among the world’s most ambitious artists since 1584. Our custom, solid wood shadowbox frames are handcrafted in America by Larson-Juhl, innovating since 1893 and the first custom framers to establish sustainable initiatives.</p>
						<p>Your embossed, date-stamped and framed photograph is professionally packed, protected and delivered to your doorstep, ready to hang (proper weight distribution brackets included).</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="wow fixed med-pad">
		<div class="full-tile-msg vert-mid">
			<h1 data-ix="fade-up">WOW.</h1>
			<h4 data-ix="fade-up">that about sums it up.</h4>
		</div>
	</div>
<!--
	<div class="wow-copy scroll">
		<div class="half-screen half-left">
			<div class ="vert-mid half-text">
				<h3 class="altogether" data-ix="fade-in-from-left">alto<br>gether<br>diff<br>erent</h3>
        <h4 class="left-align" data-ix="fade-in-from-left">yet all together</h4>
        <p class="style-type" data-ix="fade-in-from-left">At the intersection of ”art for everyone” and the rarefied world of “important art,” is a road less traveled. &nbsp;
          <br>
          <br> That’s our road - our concept. &nbsp;
          <br>
          <br> Power to the photo. Power to photographers. Power to the people.&nbsp;
          <br>
          <br> We’re about the rebels and the risk takers, the undeniable and the unpretentious, contrast and complement and the right place at the right time.&nbsp; &nbsp;
          <br>
          <br> We are the kindred spirit of many people, from all walks of life, together in our experience of the singular power of the photograph — as our own, and in bigger context.
          <br>
          <br> We say, “Let’s go!” to becoming part of something bigger.</p>
			</div>
		</div>
		<div class="half-screen half-right">
			<div class="vert-mid">
				<img src="<?php bloginfo('template_url'); ?>/images/Deep-Red-(Monolith)-DDeWaters-2013_small.jpg" alt="Captured 52 Monolith">
			</div>
		</div>
	</div>
-->
	<div class="style fixed med-pad">
		<div data-ix="fade-in-from-right" class ="section-title">
			<h4>One week per image.<br>
			<strong class="important">Then it's gone.</strong></h4>
		</div>	
	</div>
	
	<div class="message-block">
		<div data-ix="fade-in-from-left" class="message-inside">
			<p>We believe that in love, as in art - instinct is enough. Either it draws you in or it doesn’t. If you feel the pull - from some intuitive place - that's big and that’s why you buy art. If you don't, well check back next week.</p>
            <p>With every photograph we sell during the 15/16 season (between 5.2.15 & 5.1.16) we’ll include at season's end, our large format Journal of all 52 images including, from the pen of each photographer, the story of their photograph.</p>         
            <p>At the heart of what we do, is a global community connected by the common bond of 52 Captured52 images — every photograph and every person, a part of the story, integral to the journey. And 16/17 begins.</p>
		</div>
	</div>

	<div class="bebold med-pad" data-ix="header-fade-in">
		<div class="dummy"></div>
		<div class="airplane-hangar" style="background-image: url(/wp-content/themes/captured/images/C_1501_crisp.jpg);">
		 <div class="air-mid" data-ix="fade-up">
			<h1 data-ix="fade-up">be bold.</h1>
		</div>
		</div>
	</div>

			<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
