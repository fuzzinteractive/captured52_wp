<?php
/**
 * Template Name: Manifesto Template
 *
 *
 * @package captured
 */

get_header(); ?>

		<?php while ( have_posts() ) : the_post(); ?>
		
		<div class="manifesto fixed med-pad">
			<div class ="section-title">
				<h1>ALTOGETHER<br>DIFFERENT<h1>
				<h4>YET ALL TOGETHER</h4>
			</div>	
			<div class="down-btn-white"><a href="#message" class="icon">o</a></div>		
		</div>


		<div id="message" class="message-block">
			<div data-ix="fade-in-from-left" class="message-inside">
				<p>At the intersection of "art for everyone" and the rarefied world of "important art," is a road less traveled.</p>
				<p>That’s our road - our concept.</p>
				<p>Power to the photo. Power to photographers. Power to the people.</p>
				<p>We’re about the rebels and the risk takers, the undeniable and the unpretentious, contrast and complement and the right place at the right time.</p>
				<p>We are the kindred spirit of many people, from all walks of life, together in our experience of the singular power of the photograph — as our own, and in bigger context.</p>
				<p>We say, "Let’s go!" to becoming part of something bigger.</p>

			</div>
		</div>

			<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
