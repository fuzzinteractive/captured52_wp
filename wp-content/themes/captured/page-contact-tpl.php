<?php
/**
 * Template Name: Contact Template
 *
 *
 * @package captured
 */

get_header(); ?>

			<?php while ( have_posts() ) : the_post(); ?>

	  <div class="section">
	    <div class="w-container contact-copy vert-mid">
	      <div class="white-copy-contain">
	        <h3 class="contact-header">Hello. <br>Nice to meet you.</h3>
	        <div class="line dark-line"></div>
	        <div class="highlight-link">
	          <h4 class="contact-links"><strong>e</strong> I <a href="mailto:peter@captured52.com" class="contact-link">peter@captured52.com</a></h4>
	        </div>
	        <div class="highlight-link">
	          <h4 class="contact-links"><strong>p</strong> I <a href="tel:3038808894" class="contact-link">303 880 8894</a></h4>
	        </div>
	      </div>
	    </div>
	  </div>

			<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
