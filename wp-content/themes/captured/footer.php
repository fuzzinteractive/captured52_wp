<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package captured
 */
?>

</div><!--site-wrapper-->
<?php if(!is_front_page()) { ?>
<script>
	jQuery(window).load(function() {
 		jQuery('.site-wrapper').addClass('loaded');
 	});
 </script>
<?php } else {} ?>

<?php wp_footer(); ?>


</body>
</html>