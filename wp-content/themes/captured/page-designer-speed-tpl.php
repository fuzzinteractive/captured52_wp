<?php
/**
 * Template Name: Designer Speed Template
 *
 *
 * @package captured
 */

get_header(); ?>



<?php if ( is_user_logged_in() ) { ?>

    <div class="upcoming-wrap">
      <div class="w-contain">
      <?php
      $args = array(
      'post_type' => 'photo',
      'posts_per_page' => -1     
      );
    $the_query = new WP_Query( $args ); ?>

      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <?php 
        $price = get_field('price');
        $attachment_id = get_field('image');
        $size_full = "full";
        $size_small = "sm-dev";
        $image_full = wp_get_attachment_image_src( $attachment_id, $size_full );
        $image_sm = wp_get_attachment_image_src( $attachment_id, $size_small );
        ?>
	     <div id="upcoming-item-<?php the_ID(); ?>" class="w-col w-col-4 upcoming-item proportion-item" style="background-image: url('<?php echo $image_sm[0]; ?>');">
        <div class="upcoming-inside">
          <div class="upcoming-content">
            <div class="bkg-dark-full"></div>
            <a href="#" data-reveal-id="upcoming-pop" data-lg-img="<?php echo $image_full[0]; ?>" class="upcoming-button uc-img-button">+</a>
            <div class="over-bkg">
              <h2><?php the_field('photographer'); ?></h2>
              <h3><?php the_title(); ?></h3>
              <span class="upcoming-size"><?php the_field('size'); ?> | <?php echo $price; ?></span>         
              <a href="#" data-reveal-id="notify-form-modal" data-photo="<?php the_title(); ?>"  class="button upcoming-button notify-button">Notify Me When Available</a>
            </div>
          </div>
        </div>
       </div>
     
    

			<?php endwhile; // end of the loop. ?>
        </div>
     </div>


     <!--modals-->
      <div id="notify-form-modal" class="reveal-modal">
        <?php echo do_shortcode( '[contact-form-7 id="158" title="Notification"]' ); ?> 
         <a class="close-reveal-modal">&#215;</a>  
      </div>

    

      <script>
        jQuery('a.uc-img-button').on('click', function(e){
            
            var lgImg = jQuery(this).data('lg-img');
           
            var imgUrl = '<img class="blow-up" src="'+lgImg+'" />'
            //var iFrameCode = '<h1>test</h1>';
            jQuery('.lg-img-modal-content').html(imgUrl);
            console.log(imgUrl);
          });
          /*jQuery('#contact-page-sponsor-modal-2').on('hidden.bs.modal', function () {
            jQuery('#contact-page-sponsor-modal-2').html('');
          });*/
        </script>


        <div id="upcoming-pop" class="reveal-modal zoomed upcoming-modal">
          <div class="lg-img-modal-content">
           
          </div>
          <a class="close-reveal-modal">&#215;</a>
        </div>

  

   <?php } else {  ?>
   
    <!--<div class="login-form-contain">
      <div class="nda-checkbox-contain">
        <input type="checkbox" id="nda-check" name="nda" value="Agree"><span>I agree to the <a href="#" data-reveal-id="nda-modal">Terms of Service</a></span>
      </div>
     
    </div>-->

     <?php
          $args = array(
          'page_id' => 255,
          'posts_per_page' => 1
          
        );
        $the_query = new WP_Query( $args ); ?>

      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

    
      <div class="nda-content" id="post-<?php the_ID(); ?>">
      
        <div class="designer-text-contain">
          <!--<h1><em>Hello,</em></h1>
          <p>Representing the most striking large format photographs in the world, Captured52 was founded on the idea that the right photograph, when viewed on a grand scale, completely transforms the experience.</p>
          <p>From 52 invited photographers from around the world, our images represent a broad cross section of engaging, adventurous and culturally relevant work, as diverse as it is unexpected.</p>
          <p>From contrast to complement across a dynamic range of styles and aesthetic sensibilities, a Captured52 image is the antithesis of homogenized and uninspired. We believe that powerful photography, energizes emotions and space —  memorable and defining visual statements that reinforce brand identity. </p>
          <p>To the consumer, we offer 52 images that are published as open editions and printed exclusively on Hahnemühle 100% Photo Rag paper, shadowbox museum framed in Italian Roma moulding and offered in only one magnificent size between 40” x 40” and 60” x 80.” Each image is offered for one week only - then it's gone.</p>
          <p>For designers, architects and advisors, this portfolio of 52 images serves as a baseline from which we can tailor to your scope, vision and budget — from a single image to a collection built around a specific photographer or multiple photographers and/or specifically sourced from our extensive database of international photographers.</p>
          <p>All of our original photographs are printed directly from the master files of each photographer, framed to exacting specifications and shipped from our studio in Portland, Oregon. Since we do everything in-house, we can cost effectively customize around volume, size, substrate and framing to meet your needs.</p>
          <p>We hope to become your trusted source for the finest large scale original photographs available today and welcome the opportunity to find the most compelling work for your residential, commercial or hospitality projects.</p>          
          <p>Please contact me directly at <a style="color: #fff;" href="mailto:peter@captured52.com" target="_blank">peter@captured52.com</a> or at <a style="color: #fff;" href="tel:303.880.8894">303.880.8894</a>.</p>
          <p>Thank you,</p>
          <p>Peter Johnson</p>-->

     

          <?php the_content(); ?>

          <?php if (function_exists('pol_showform')) pol_showform(); ?>
        </div>
      </div>
       
 
      <?php endwhile; ?>

  <?php } ?>

<?php get_footer(); ?>
