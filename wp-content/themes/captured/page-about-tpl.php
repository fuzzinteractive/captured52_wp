<?php
/**
 * Template Name: About Template
 *
 *
 * @package captured
 */

get_header(); ?>

			<?php while ( have_posts() ) : the_post(); ?>

  <div class="about-captured">
    <div class="w-container">
      <div class="white-copy-contain">
        <p class="about-txt">Captured52 was born from my interest in photography as contemporary art. However, I always questioned the “value” of limiting a photograph with arbitrary edition sizes, print sizes and pricing structures. Confusing at best and not altogether convincing.
          <br>
          <br> Wouldn’t it stand to reason that more people owning an original photograph, not fewer — further substantiates the work?&nbsp;
          <br>
          <br> With the help of some very talented people we arrived at a simple solution. Sell only big, commanding work. One size, framed and available to all. At a great price.&nbsp;&nbsp;
          <br>
          <br> Unlike any other art form, photographs connect us to our world and to each other—human-to-human. So ask yourself, “Do I like it?” “Can I afford it?” and “Will it fit?”&nbsp;
          <br>
          <br>Simple.
          <br>
          <br>Peter Johnson
          <br>5.2.15&nbsp;
          <br>Portland</p>
      </div>
    </div>
  </div>

			<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
