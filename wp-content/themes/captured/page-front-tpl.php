<?php
/**
 * Template Name: Home Page Template
 *
 *
 * @package captured
 */

get_header(); ?>

			<?php while ( have_posts() ) : the_post(); ?>
				<?php
				$attachment_id = get_field('background_image');
				$full = "full"; // (thumbnail, medium, large, full or custom size)

				$sm_dev = "sm-dev";
				$image_full = wp_get_attachment_image_src( $attachment_id, $full );

				$image_sm = wp_get_attachment_image_src( $attachment_id, $sm_dev );
				?>
				<style>
				.home-section {
					background-image: url('<?php echo $image_full[0]; ?>')
				}

				@media all and (max-width: 480px){
					.home-section {
						background-image: url('<?php echo $image_sm[0]; ?>')
					}
				}
                    
                    .home-title-contain h1 {
                        color: <?php the_field('title_color'); ?>;
                    }
                    
                    .is-countdown {
                        color: <?php the_field('countdown_color'); ?>;
                    }
                    
				</style>

				<script>
				jQuery(document).ready(function() {
					jQuery('#finalcountdown').countdown({until: new Date(<?php the_field('countdown'); ?>)});
				});
				</script>



				<div class="section hundo home-section">
					<!--Entrance Curtain-->
					<!--<div class="curtain">
						<div class="inside-curtain vert-mid">
							<a class="button">Enter Here</a>
						</div>
					</div>-->
				</div>

				<a class="home-link-layer" href="<?php the_field('product_link'); ?>"></a>
				<div class="w-container home-container hundo">					
					<div class="home-title-contain">
						<h1><span class="title-text title-1">This </span><span class="title-text title-2">Is </span><span class="title-text title-3 sans-serif">N&deg; <?php the_field('week_number'); ?></span></h1>
						<span class="title-text title-4 buy-btn">
							<div class="centered-bar">
								<a class="button black-btn" href="<?php the_field('product_link'); ?>">show me</a>
							</div>
						</span>	
						<div class="count-contain"><div id="finalcountdown"></div></div>
					</div>
					<div class = "fs-trigger">
						<img src="<?php echo get_template_directory_uri() ?>/images/fullscreen-icon.png" alt="Enter Fullscreen">
					</div>
				</div>
				


				<div class="credits-bar">
				    <div class="w-row byline">
				      	<div class="w-col w-col-2">
							<a class="icon" href="https://www.facebook.com/Captured52" target="_blank">w</a>
							<a class="icon" href="https://twitter.com/Captured52" target="_blank">x</a>
							<a class="icon" href="https://instagram.com/captured52/" target="_blank">v</a>
						</div>
                        <div class="w-col w-col-6">
                            
                                <!-- Begin MailChimp Signup Form -->
                            
                                <div id="mc_embed_signup">
                                <form action="//captured52.us9.list-manage.com/subscribe/post?u=6d6782d1dedf72e31aca5fd32&amp;id=e29482cbda" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                    <div id="mc_embed_signup_scroll">
                                    <h5 class="signup-header">FIRST LOOK WEEKLY</h5>
                                <div class="mc-field-group">
                <input type="text" name="EMAIL" onclick="this.value='';" onfocus="this.select()" onblur="this.value=!this.value?'Enter Email':this.value;" value="Enter Email" class="required email" id="mce-EMAIL" />
                                </div>
                                    <div id="mce-responses">
                                        <div class="response" id="mce-error-response" style="display:none"></div>
                                        <div class="response" id="mce-success-response" style="display:none"></div>
                                    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                    <div style="position: absolute; left: -5000px;"><input type="text" name="b_6d6782d1dedf72e31aca5fd32_e29482cbda" tabindex="-1" value=""></div>
                                    <div><input type="submit" value="Sign Up" name="sign up" id="mc-embedded-subscribe" class="button"></div>
                                    </div>
                                </form>
                                </div>

                                <!--End mc_embed_signup-->
                            
                        </div>
				      	<div class="w-col w-col-4">
				        	<div class="byline-title"><?php the_field('footer_text'); ?></div>
				      	</div>
				   	</div>
				</div>

				<img id="fullscreen" src="<?php echo $image_full[0]; ?>">


			<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
