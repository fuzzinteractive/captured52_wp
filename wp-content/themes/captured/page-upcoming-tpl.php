<?php
/**
 * Template Name: Upcoming Template
 *
 *
 * @package captured
 */

get_header(); ?>

    <div class="upcoming-wrap">
      <div class="w-contain">
      <?php
      $args = array(
      'post_type' => 'photo',
      'posts_per_page' => -1,
      'tax_query' => array(
        array(
          'taxonomy' => 'photo_cat',
          'field'    => 'slug',
          'terms'    => 'upcoming',
        ),
      ),
    );
    $the_query = new WP_Query( $args ); ?>

      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <?php 
        $price = get_field('price');
        $attachment_id = get_field('image');
        $size_full = "full";
        $size_small = "sm-dev";
        $image_full = wp_get_attachment_image_src( $attachment_id, $size_full );
        $image_sm = wp_get_attachment_image_src( $attachment_id, $size_small );
        ?>
	     <div id="upcoming-item-<?php the_ID(); ?>" class="w-col w-col-4 upcoming-item proportion-item" style="background-image: url('<?php echo $image_sm[0]; ?>');">
        <div class="upcoming-inside">
          <div class="upcoming-content">
            <div class="bkg-dark-full"></div>
            <a href="#" data-reveal-id="upcoming-<?php the_ID(); ?>" class="upcoming-button uc-img-button">+</a>
            <div class="over-bkg">
              <h2><?php the_field('photographer'); ?></h2>
              <h3><?php the_title(); ?></h3>
              <span class="upcoming-size"><?php the_field('size'); ?> | <?php echo $price; ?></span>         
              <a href="#" data-reveal-id="notify-form-modal" data-photo="<?php the_title(); ?>" class="button upcoming-button notify-button">Notify Me When Available</a>
            </div>
          </div>
        </div>
       </div>
     
    

			<?php endwhile; // end of the loop. ?>
        </div>
     </div>


     <!--modals-->
      <div id="notify-form-modal" class="reveal-modal">
        <?php echo do_shortcode( '[contact-form-7 id="158" title="Notification"]' ); ?> 
         <a class="close-reveal-modal">&#215;</a>  
      </div>

      <?php wp_reset_query(); // reset the query ?>
       <?php
          $args = array(
          'post_type' => 'photo',
          'posts_per_page' => -1,
          'tax_query' => array(
            array(
              'taxonomy' => 'photo_cat',
              'field'    => 'slug',
              'terms'    => 'upcoming',
            ),
          ),
        );
        $the_query = new WP_Query( $args ); ?>

      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
         <?php 
          $attachment_id = get_field('image');
          $size_full = "full";
          $size_small = "sm-dev";
          $image_full = wp_get_attachment_image_src( $attachment_id, $size_full );
          $image_sm = wp_get_attachment_image_src( $attachment_id, $size_small );
        ?>

        <div id="upcoming-<?php the_ID(); ?>" class="reveal-modal zoomed upcoming-modal">
        <img class="blow-up" src="<?php echo $image_full[0]; ?>" />
        <a class="close-reveal-modal">&#215;</a>
  </div>

      <?php endwhile; // end of the loop. ?>
     

<?php get_footer(); ?>
