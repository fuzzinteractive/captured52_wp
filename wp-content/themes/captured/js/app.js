jQuery(document).ready(function(){

  /*hamburger nav */
  document.querySelector( "#nav-toggle" ).addEventListener( "click", function() {
    this.classList.toggle( "active" );
    jQuery('.nav-open').fadeToggle();
  });

  /*full screen*/
  jQuery('.fs-trigger').on('click', function(){
        var elem = document.getElementById('fullscreen');
        if(document.webkitFullscreenElement) {
            document.webkitCancelFullScreen();
            jQuery('#fullscreen').removeClass('see');
        }
        else {
            jQuery('#fullscreen').addClass('see');
            elem.webkitRequestFullScreen();
        };
    });

  /*smooth scroll*/
  jQuery('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
        || location.hostname == this.hostname) {

        var target = jQuery(this.hash);
        target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             jQuery('html,body').animate({
                 scrollTop: target.offset().top
            }, 1000);
            return false;
        }
    }
  });

  /*Free Shipping*/

  jQuery('span.shopp-cart.cart-shipping').html('<span class="free-shipping">FREE</span>');

  /*Notify me form */

  jQuery('a.upcoming-button').on('click', function(e){
    var photo = jQuery(this).data('photo');
    jQuery('input#photo-id').val('');
    jQuery('input#photo-id').val(photo);
    console.log(photo);
  });

  /* nda checkbox */
  jQuery('.login-form-contain #loginform').submit(function() {
    if (jQuery('#nda-check').prop('checked')) {
      // everything's fine...
    } else {
    alert('You must agree to the Captured52 NDA to view');
    return false;
    }
});

  


});

