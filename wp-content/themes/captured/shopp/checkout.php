<div class="checkout-form-contain w-container">
	<form action="<?php shopp( 'checkout.url' ); ?>" method="post" class="shopp validate" id="checkout">

		<?php shopp( 'checkout.cart-summary' ); ?>

		<?php if ( shopp( 'cart.hasitems' ) ) : ?>

			<?php shopp( 'checkout.function' ); ?>


				<?php if ( shopp( 'customer.notloggedin' ) ) : ?>
					<li>
						<label for="login"><?php _e('Login to Your Account','Shopp'); ?></label>
						<span><label for="account-login"><?php _e('Email','Shopp'); ?></label><?php shopp('customer','account-login','size=20&title='.__('Login','Shopp')); ?></span>
						<span><label for="password-login"><?php _e('Password','Shopp'); ?></label><?php shopp('customer','password-login','size=20&title='.__('Password','Shopp')); ?></span>
						<span><?php shopp('customer','login-button','context=checkout&value=Login'); ?></span>
					</li>
				<?php endif; ?>

				<div class="w-row">
					<h3><?php _e('Contact Information','Shopp'); ?></h3>
					<div class="w-col w-col-4">
						<span><?php shopp('checkout','firstname','required=true&placeholder=First&minlength=2&size=8&title='.__('First Name','Shopp')); ?></span>
					</div>
					<div class="w-col w-col-4">
						<span><?php shopp('checkout','lastname','required=true&placeholder=Last&minlength=2&size=14&title='.__('Last Name','Shopp')); ?></span>
					</div>
					<div class="w-col w-col-4">
						<span><?php shopp('checkout','company','size=22&placeholder=Company&title='.__('Company/Organization','Shopp')); ?></span>
					</div>
				</div>



				<div class="w-row">
					<div class="w-col w-col-4">
						<span><?php shopp('checkout','phone','format=phone&placeholder=Phone&size=15&title='.__('Phone','Shopp')); ?></span>
					</div>
					<div class="w-col w-col-4">
						<span><?php shopp('checkout','email','required=true&placeholder=Email&format=email&size=30&title='.__('Email','Shopp')); ?></span>
					</div>
				</div>

				<?php if ( shopp( 'customer.notloggedin' ) ) : ?>
					<li>
						<span><label for="password"><?php _e('Password','Shopp'); ?></label>
						<?php shopp('checkout','password','required=true&format=passwords&size=16&title='.__('Password','Shopp')); ?></span>

						<span><label for="confirm-password"><?php _e('Confirm Password','Shopp'); ?></label>
						<?php shopp('checkout','confirm-password','required=true&format=passwords&size=16&title='.__('Password Confirmation','Shopp')); ?></span>
					</li>
				<?php endif; ?>

				<?php if ( shopp( 'cart.needs-shipped' ) ) : ?>
					<!--<li class="half" id="billing-address-fields">-->
				<?php else: ?>
					<!--<li>-->
				<?php endif; ?>
				<div class="w-row">
					<div class="w-col-6 w-col">
						<h3><?php _e( 'Billing Address', 'Shopp' ); ?></h3>


						<?php shopp( 'checkout.billing-name', 'required=false&placeholder=' . __( 'Name', 'Shopp' ) .'&title=' . __( 'Bill to', 'Shopp' ) ); ?>


						<?php shopp( 'checkout.billing-address', 'required=true&placeholder=' . __( 'Street Address', 'Shopp' ) .'&title=' . __( 'Billing street address', 'Shopp' ) ); ?>

						<?php shopp( 'checkout.billing-xaddress', 'placeholder=' . __( 'Address Line 2', 'Shopp' ) .'&title=' . __( 'Billing address line 2', 'Shopp' ) ); ?>

						<?php shopp( 'checkout.billing-city', 'required=true&placeholder=' . __( 'City', 'Shopp' ) .'&title=' . __( 'City billing address', 'Shopp' ) ); ?>

						<?php shopp( 'checkout.billing-state', 'required=auto&title=' . __( 'State/Province/Region billing address', 'Shopp' ) ); ?>

						<?php shopp( 'checkout.billing-postcode', 'required=true&placeholder=' . __( 'Postal / Zip Code', 'Shopp' ) .'&title=' . __( 'Postal/Zip Code billing address', 'Shopp' ) ); ?>


						<?php shopp( 'checkout.billing-country', 'required=true&title=' . __( 'Country billing address', 'Shopp' ) ); ?>

						<?php if ( shopp( 'cart.needs-shipped' ) ) : ?>
							<div class="same-shipping">
								<?php shopp( 'checkout.same-shipping-address' ); ?>
							</div>
						</div>

					<div class="w-col-6 w-col" id="shipping-address-fields">
						<h3><?php _e( 'Shipping Address', 'Shopp' ); ?></h3>

							<?php shopp( 'checkout.shipping-name', 'required=false&placeholder=' . __( 'Name', 'Shopp' ) .'&title=' . __( 'Ship to', 'Shopp' ) ); ?>

							<?php shopp( 'checkout.shipping-address', 'required=true&placeholder=' . __( 'Street Address', 'Shopp' ) .'&title=' . __( 'Shipping street address', 'Shopp' ) ); ?>

							<?php shopp( 'checkout.shipping-xaddress', 'placeholder=' . __( 'Street Address', 'Shopp' ) .'&title=' . __( 'Shipping address line 2', 'Shopp' ) ); ?>

							<?php shopp( 'checkout.shipping-city', 'required=true&placeholder=' . __( 'City', 'Shopp' ) .'&title=' . __( 'City shipping address', 'Shopp' ) ); ?>

							<?php shopp( 'checkout.shipping-state', 'required=auto&placeholder=' . __( 'State / Province', 'Shopp' ) .'&title=' . __( 'State/Provice/Region shipping address', 'Shopp' ) ); ?>

							<?php shopp( 'checkout.shipping-postcode', 'required=true&placeholder=' . __( 'Postal / Zip Code', 'Shopp' ) .'&title=' . __( 'Postal/Zip Code shipping address', 'Shopp' ) ); ?>

							<?php shopp( 'checkout.shipping-country', 'required=true&title=' . __( 'Country shipping address', 'Shopp' ) ); ?>

					</div>
				<?php else: ?>
					<!--</li>-->
				<?php endif; ?>
			</div>
				<?php if ( shopp( 'checkout.billing-localities' ) ) : ?>
					<li class="half locale hidden">
						<div>
							<label for="billing-locale"><?php _e( 'Local Jurisdiction', 'Shopp' ); ?></label>
							<?php shopp( 'checkout.billing-locale' ); ?>
						</div>
					</li>
				<?php endif; ?>

				<div class="stripe-contain">
					<?php shopp( 'checkout.payment-options' ); ?>
					<?php shopp( 'checkout.gateway-inputs' ); ?>
				</div>

				<?php if ( shopp( 'checkout.card-required' ) ) : ?>
					<li class="payment">
						<label for="billing-card"><?php _e( 'Payment Information', 'Shopp' ); ?></label>
						<span>
							<label for="billing-card"><?php _e( 'Credit/Debit Card Number', 'Shopp' ); ?></label>
							<?php shopp( 'checkout.billing-card', 'required=true&size=30&title=' . __( 'Credit/Debit Card Number', 'Shopp' ) ); ?>
						</span>
						<span>
							<label for="billing-cvv"><?php _e( 'Security ID', 'Shopp' ); ?></label>
							<?php shopp( 'checkout.billing-cvv', 'size=7&minlength=3&maxlength=4&title=' . __( 'Card\'s security code (3-4 digits on the back of the card)', 'Shopp' ) ); ?>
						</span>
					</li>
					<li class="payment">
						<span>
							<label for="billing-cardexpires-mm"><?php _e('MM','Shopp'); ?></label>
							<?php shopp( 'checkout.billing-cardexpires-mm', 'required=true&minlength=2&maxlength=2&title=' . __( 'Card\'s 2-digit expiration month', 'Shopp' ) ); ?> /
						</span>
						<span>
							<label for="billing-cardexpires-yy"><?php _e( 'YY', 'Shopp' ); ?></label>
							<?php shopp( 'checkout.billing-cardexpires-yy', 'required=true&minlength=2&maxlength=2&title=' . __( 'Card\'s 2-digit expiration year', 'Shopp' ) ); ?>
						</span>
						<span>
							<label for="billing-cardtype"><?php _e( 'Card Type', 'Shopp' ); ?></label>
							<?php shopp( 'checkout.billing-cardtype', 'required=true&title=' . __( 'Card Type', 'Shopp' ) ); ?>
						</span>
					</li>
					<?php if ( shopp( 'checkout.billing-xcsc-required' ) ) : // Extra billing security fields ?>
						<li class="payment">
							<span>
								<label for="billing-xcsc-start"><?php _e( 'Start Date', 'Shopp' ); ?></label>
								<?php shopp( 'checkout.billing-xcsc', 'input=start&size=7&minlength=5&maxlength=5&title=' . __( 'Card\'s start date (MM/YY)', 'Shopp' ) ); ?>
							</span>
							<span>
								<label for="billing-xcsc-issue"><?php _e( 'Issue #', 'Shopp' ); ?></label>
								<?php shopp( 'checkout.billing-xcsc', 'input=issue&size=7&minlength=3&maxlength=4&title=' . __( 'Card\'s issue number', 'Shopp' ) ); ?>
							</span>
						</li>
					<?php endif; ?>

				<?php endif; ?>


				<div class="inline mc-opt-in">
					<label for="marketing"><?php shopp('checkout','marketing'); ?> <?php _e( 'Yes, I would like to receive e-mail updates and special offers!', 'Shopp' ); ?></label>
				</div>


			<p class="submit"><?php shopp( 'checkout.submit', 'value=' . __( 'Submit Order', 'Shopp' ) ); ?></p>

		<?php endif; ?>
	</form>
</div><!--.checkout-form-contain-->
