
<?php shopp('collection.description') ?>

<?php if ( shopp( 'collection.hasproducts', 'load=coverimages' ) ) : ?>
	<div class="category">
		<div class="product-contain" id="pack">
			<?php while( shopp( 'collection.products' ) ) : ?>
				<div class="product" itemscope itemtype="http://schema.org/Product">
				<div class="frame">
					<a href="<?php shopp( 'product.url' ); ?>" itemprop="url"><?php shopp( 'product.coverimage', 'setting=thumbnails&itemprop=image' ); ?></a>
					<a class="details" href="<?php shopp( 'product.url' ); ?>">
						<h4 class="name">
						<span itemprop="name"><?php shopp( 'product.name' ); ?></span>
						</h4>
						<h5 class="photo-title"><?php the_field('photo_title'); ?></h5>
					</a>
				</div>
			</div>
			<?php endwhile; ?>
		</div>

		<div class="alignright">
			<?php shopp( 'collection.pagination', 'show=10' ); ?>
		</div>
	</div>

<?php else : ?>
	<?php if ( ! shopp('storefront.is-landing') ) shopp( 'storefront.breadcrumb' ); ?>
	<p class="notice"><?php _e( 'No products were found.', 'Shopp' ); ?></p>
<?php endif; ?>
