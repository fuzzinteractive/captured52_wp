
<?php if ( shopp( 'product.found' ) ) : ?>


	<?php shopp('product.schema'); ?>



<?php else : ?>
	<h3><?php _e( 'Product Not Found', 'Shopp' ); ?></h3>
	<p><?php _e( 'Sorry! The product you requested is not found in our catalog!', 'Shopp' ); ?></p>
<?php endif; ?>

  <div class="w-clearfix product">


		<form action="<?php shopp( 'cart.url' ); ?>" method="post" class="shopp validate validation-alerts">


	    <div class="w-hidden-small w-hidden-tiny c-mark-contain">
	      <a class="w-inline-block" href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/c_hex_white.svg"  alt="Captured 52">
	      </a>
	    </div>
	    <div class="w-row row-100">
	      <div class="w-col w-col-6 w-col-stack prod-img">
	        <a href="#" data-reveal-id="photoModal"><img src="<?php the_field('main_product_image'); ?>" alt="<?php the_field('photo_title'); ?>" class="vert-mid"></a>
	      </div>
	      <div class="w-col w-col-6 w-col-stack black-background">
					<div class="vert-mid product-copy">
		        <div class="photo-title">
		          <h3><?php shopp( 'product.name' ); ?></h3>
		          <h4 class="image-title"><?php shopp( 'product.description' ); ?></h4>
		        </div>
		        <div class="highlight">
		          <div class="avail-text"><?php the_field('availability'); ?></div>
		        </div>
		        <h5 class="w-clearfix details-title"><span class="w-clearfix glasses-icon">q<span style="font-family: aktiv-grotesk-std, sans-serif; font-size: 1rem; letter-spacing: 0.6rem; line-height: 1rem; text-transform: uppercase;" class="det-span"><a href="#" class="details-link" data-reveal-id="detail-modal">Details</a></span></span></h5>

				<div class="w-row details <?php shopp( 'product.price' ); ?> large-details">
		          <div class="w-col w-col-3">
		            <p>Paper Size:
		              <br>Image Size:
		              <br>Paper:
		              <br>Frame:
		              <br>Price:
		              <br>Shipping:</p>
		          </div>
		          <div class="w-col w-col-9">
		            <p><em><?php the_field('paper_size'); ?><br>
								<?php the_field('image_size'); ?><br>
								<?php the_field('paper'); ?><br>
								<?php the_field('frame'); ?><br>
								<?php shopp( 'product.price' ); ?><br>
								FREE</em>
		            </p>
		          </div>
		        </div>


		        <div class="w-row details <?php shopp( 'product.price' ); ?> small-details">
		          <div class="w-col w-col-12">
		            <p>Paper Size:<br>
		            <?php the_field('paper_size'); ?></p>
		            <p>Image Size:<br>
		            <?php the_field('image_size'); ?></p>
		             <p>Paper:<br>
		             <?php the_field('paper'); ?></p>   
		             <p>Frame:<br>
		             <?php the_field('frame'); ?></p>
		             <p>Price:<br>
		             <?php shopp( 'product.price' ); ?></p>
		             <p>Shipping:<br>
		             FREE</p>
		          </div>
		        </div>

						<div class="w-row <?php shopp( 'product.price' ); ?>">
							<div class="w-col w-col-7 add-to-cart-contain">
								<div class="add-to-cart-area button">
									<span class="inline-cart-icon">k</span>
									<?php shopp( 'product', 'add-to-cart', 'redirect=checkout' ); ?>
								</div>
							</div>
						</div>
						<div class="w-row">
							<div class="bonus-blurb w-col w-col-7"><p>Every purchase includes, at season's end, our beautiful large format Journal of all 52 photographs and the story of each, by the photographer.</p></div>
						</div>
					</div>
	      </div>
	    </div>
			<div class="down-btn"><a href="#gallery" class="icon">o</a></div>
		</form>
		<div class="lifestyle-gallery" id="gallery">
		<?php
			$images = get_field('lifestyle_gallery');
			if( $images ): ?>
					<ul>
							<?php foreach( $images as $image ): ?>
									<li class="lifestyle_image">
										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									</li>
							<?php endforeach; ?>
					</ul>
			<?php endif; ?>
		</div>
  </div>


	<!--detail modal -->

	<div id="detail-modal" class="reveal-modal">
		<h4>DETAILS:</h4>
		<?php the_field('details_modal'); ?>
		<a class="close-reveal-modal button">BACK TO PHOTO</a>
	</div>

	<!--photo modal-->
	<div id="photoModal" class="reveal-modal zoomed">
	    <img class="blow-up" src="<?php the_field('zoom_image'); ?>" />
	     <a class="close-reveal-modal">&#215;</a>
	</div>
