<?php
/**
 * Template Name: FAQ Template
 *
 *
 * @package captured
 */

get_header(); ?>

			<?php while ( have_posts() ) : the_post(); ?>
				<div>
			    <div class="south"></div>
			    <div class="west"></div>
			    <div class="east"></div>
			    <div class="north"></div>
			  </div>
			  <div class="terms">
			    <div class="w-container terms-copy">
			      <div class="white-copy-contain">
			        <h4 class="dark">faq</h4>
			        <!--<p class="terms-text">Q Do you like what you do?
			          <br>A Yes we do. Do you like what we do?
			          <br>
			          <br>Q Isn’t it hard finding 52 incredible images that meet your exacting standards of quality, aesthetic value and cultural relevance?
			          <br>A Yes - very hard. Just because an image can be printed big, doesn’t mean it should be. These 52 images are culled from thousands of candidate images, reviewed, proofed at full size and finally decided on. These are your 52.
			          <br>
			          <br>Q Why don’t you reveal all 52 photographs?
			          <br>A Mystery. There’s something exciting about being at the right place at the right time - grabbing something before it slips away. And when it’s no longer available - that valuable.
			          <br>
			          <br>Q Why do you sell only open editions?
			          <br>A The visionary photographer Ansel Adams sold over 1,300 original prints of his iconic image Moonrise, Hernandez - as an open edition. Why? Because he needed the cash and there was a demand! In the early '70s you could have picked one up for $500! Now that image ranges from $80,000 - $600,000 if you can find one. Nothing’s changed - great work sold as an open edition can make more money for the photographer and provide many more people the chance to own powerful original work. Many people owning the same work, not fewer, is what substantiates true value — found in the singular power of the piece. Don’t fall for the limited edition hype.
			          <br>
			          <br>Q Do you sell only original photographs?
			          <br>A Yes. Every print that we make is an original because it is printed directly from the photographer’s master file that they send directly to us. Many of our photographers create their own artwork or performance art and then shoot it, or manipulate the original toward experimental work or create pieces that don’t even involve a camera - creating through scanning, photograms, photoshop, etc. We consider it all a part of the “photographic” process and natural evolution of the craft.
			          <br>
			          <br>Q Should I be freaking out about how big this thing is?
			          <br>A Don’t over react, be creative! You’ve got an incredible art work that could look great just leaning against the wall. Make a statement.
			          <br>
			          <br>Q Do you ship internationally?
			          <br>A Currently, we are only shipping within the U.S. We are however, happy to accommodate international requests - please shoot us an email at shipping@captured52.com.
			          <br> Every Captured image is printed to exacting specifications by our master printer, museum framed with the highest quality Roma moulding and packed with care in our custom art containers. We ship from Portland, Oregon within 2-3 weeks via UPS or FedEx (flat packed up to 40”x 60”) or ground (crated for 60” x 80”) via one of our three art handling shippers. PLEASE NOTE, THAT OUR 60” X 80” CRATES WEIGH AROUND 185LBS SO PLEASE PLAN AHEAD AND PREPARE ACCORDINGLY.
			          <br>
			          <br> Delivery times will vary, depending on our carrier. When your image ships, you will receive a shipment confirmation email including your tracking number so that you can follow your image as it travels home!
			          <br>
			          <br> ALL SHIPMENTS MUST BE ACCEPTED AND SIGNED UPON DELIVERY.
			          <br>
			          <br>Q Can I expect my Captured image to be the same as I see it on my computer screen.
			          <br>A Images will typically appear brighter on your computer screen because they are backlit. We make every effort to display as close to what your actual image will look like and minimize variations in final printing.
			          <br>
			          <br>Q What is your return policy?
			          <br>A Because we custom print and frame your image, we do not accept returns. We are however happy to exchange for another image of the same or lesser value. If for any reason you are not satisfied with your purchase of a Captured image, you may return it within seven (7) days of receipt and and select another image from our Archive or future image. Original shipping charges will not be refunded, return shipment costs are the responsibility of the customer and Captured will ship the replacement image at no charge. If you do decide to return your Captured Image, please follow these instructions:
			          <br>
			          <br>Email returns@captured52.com with your order number and specific reason for the return.
			          <br>Captured will respond with an Exchange Merchandise Authorization number (EMA#)
			          <br>Please print the Exchange Authorization form, complete it, including your EMA# and enclose in the return shipment of the artwork.
			          <br>
			          <br>Exchange Authorization.doc
			          <br>
			          <br>The framed image must be packed in the original shipping box or crate with the Exchange Authorization Form, Captured branded insert artwork, Thank You Card from the photographer and image detail sticker on the back of the frame. No exchange will be accepted without the proper paperwork and the image will need to be returned in perfect condition in it’s original packaging to qualify for an exchange.
			          <br>
			          <br>Ship it back to: Captured I 2044 NW Pettygrove St. I Portland OR, 97209
			          <br>
			          <br>Damaged Prints
			          <br>In the unfortunate event that your order arrives in less than satisfactory condition, simply take one digital photo of the item and one of the shipping container it arrived in, then attach the photos to an email and send to returns@captured52.com. We’ll get back to you in 2 business days on how to proceed with your order.
			          <br>For any additional issues, please contact customer service a service@captured52.com.</p>-->



						<p>Q I thought interpreting photographs as art required a Phd. Doesn’t your approach fly in the face of conventional wisdom?<br>
						A Arriving at simple, is complex and we felt that reengaging interest in how photographs 	are perceived, valued and transacted as contemporary art would require a radical 	departure from the status quo. At its most elemental - art is about beauty, emotion and 	meaning.</p>
											
						<p>Q	Isn’t it hard finding 52 world class images that meet your exacting standards of quality, 	aesthetic value and cultural relevance?<br>
						A	Yes - very hard. Just because an image can be printed big, doesn’t mean it should be. 	These 52 images are culled from thousands of candidate images, reviewed, proofed at 	full size and finally decided on. These are your 52. 
						</p>

						<p>Q	Why don’t you reveal all 52 photographs?<br>
						A 	Mystery. There’s something exciting about being at the right place at the right time — 	grabbing something before it slips away. And when it’s no longer available, that’s 	valuable.
						</p>

						<p>Q	Why do you sell only open editions?<br>
						A	The visionary photographer Ansel Adams sold over 1,300 original prints of his iconic 	image, Moonrise, Hernandez - as an open edition. Why? Because he needed the cash and 	there was a demand. In the early '70s you could have picked one up for $500! Now that 	image ranges from $80,000 to $600,000 — if you can find one. Nothing’s changed - 	great work sold as an open edition can make more money for the photographer and 	provide many more people the chance to own original work at a fair price. While many 	people 	owning the same work, not fewer, is what substantiates the work — true value, 	is found in the singular power of the piece. Don’t fall for the limited edition hype.
						</p>

						<p>Q	Do you sell only original photographs?<br>
						A	Yes. Every image that we publish is printed directly from the photographer’s master file 	that they send directly to us. Not only are our photographs original, so too the creative 	process — from veterans to a new generation of artists experimenting across 	traditional, sculptural, conceptual and performance art, cross pollinating analog and 	digital, and exploring the far reaches of the imagination. We consider every 	interpretation of the “photographic” 	process as original and a natural evolution of the 	craft.
						</p>

						<p>Q	What’s with all the hype around the “Letter” or “Certificate” of Authenticity?<br>
						A	Hogwash. These documents in no way prove that your art is “authentic”. The size of a 	Captured52 image however, requires a very big file from which to print at the quality we 	do. You can rest assured that the print file came directly from the photographer who has 	also approved our embossed seal located on the lower right boarder of their image. 	Authentic to the core, guaranteed.
						</p>

						<p>Q	Why do you sell the photograph in only one size?<br>
						A	Certain photographs are simply meant to be experienced big and we feel that our 52 are 	best represented on a grand scale. By offering an image exclusive to one size, we don’t 	prevent our photographers from using their image in other ways, that may 	additionally benefit them, so long as the image is smaller than the 	Captured52 image.  
						</p>

						<p>Q	Should I be freaking out about how big this thing is?<br>
						A	Don’t over react, be creative! You’ve got an incredible work of art that could look great 	just leaning against the wall. Make a statement.
						</p>

						<p>Q	How do you ship?<br>
						A	Every Captured52 image is printed to exacting specifications by our master printer, 	museum framed with the highest quality Larson-Juhl moulding and packed with care in 	our custom art containers. We ship from Portland, Oregon within 2-3 weeks via UPS or 	FedEx (flat packed up to 40”x 60”) or ground (crated for 60” x 80”) via one of several 	art handling shippers. PLEASE NOTE, THAT OUR 60” X 80” CRATES WEIGH AROUND _	185LBS SO PLEASE PLAN AHEAD AND PREPARE ACCORDINGLY. Delivery times will vary, depending on our carrier. When your image ships, you will 	receive a shipment confirmation email including your tracking number so that you can follow your image as it travels home!<br>
						ALL SHIPMENTS MUST BE ACCEPTED AND SIGNED UPON DELIVERY.
						</p>

						<p>Q	Do you ship internationally?<br>
						A	Yes, we’re happy to accommodate international requests - please shoot us an email at 	<a href="mailto:shipping@captured52.com">shipping@captured52.com</a>.
						</p>

						<p>Q	Can I expect my Captured52 image to be the same as I see it on my computer screen.<br>
						A	Images will typically appear brighter on your computer screen because they are backlit. 	We make every effort to display as close to what your actual image will look like and 	minimize variations in final printing.   
						</p>

						<p>Q 	What is your return policy?<br>
						A 	Because we custom print and frame your image, we do not accept returns. We are 	however, happy to exchange for another image of the same or lesser value. If for any 	reason 	you are not satisfied with your purchase of a Captured52 image, you may return it 	within seven (7) days of receipt and select another image from our Archive or a future 	image from the same season in which you purchased. Original shipping charges will not 	be refunded, return shipment costs are the responsibility of the customer and  Captured52	will ship the replacement image at no charge. If you do decide to return your Captured52 Image, please follow these instructions:</p>
						<ul>
							<li>Email <a href="mailto:exchange@captured52.com">exchange@captured52.com</a> with your order number and specific reason for the return.</li>
							<li>Captured52 will respond with an Exchange Image Authorization number (EIA#)</li>
							<li>Please print the Exchange Image Authorization form, complete it, including your EMA# and enclose in the return shipment of the artwork.<br>
							<a href="<?php bloginfo('template_url'); ?>/documents/Exchange_Authorization.pdf" target="_blank">Exchange Image Authorization.doc</a></li>
						</ul>
						<p>The framed image must be packed in the original shipping box or crate with the Exchange Image Authorization Form, Captured52 branded insert artwork and image detail sticker on the back of the frame. No exchange will be accepted without the proper paperwork and the image will need to be returned in perfect condition in it’s original packaging to qualify for an exchange.</p>
						<p>Ship it back to: Captured52 I 2044 NW Pettygrove St. I  Portland OR, 97209 </p>
						<p><strong>Damaged Prints</strong></p>
						<p>In the unfortunate event that your order arrives in less than satisfactory condition, simply take one digital photo of the item and one of the shipping container it arrived in, then attach the photos to an email and send to exchange@captured52.com. We’ll get back to you within 2 business days on how to proceed with your order.</p>
						<p>For any other issues, please contact customer service at <a href="mailto:service@captured52.com">service@captured52.com</a>.</p>


			      </div>
			    </div>
			  </div>

			<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
